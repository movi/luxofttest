﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Core;
using Server.Core.Interfaces;

namespace Server.WebApi.Controllers
{
    [Route("api/msg")]
    public class MessageController : Controller
    {
        private IMessagesRepository _repository;

        public MessageController(IMessagesRepository repository)
        {
            _repository = repository;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return _repository.ReadAllLines();
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
            var ip = Request.HttpContext.Connection.RemoteIpAddress;
            var time = DateTime.UtcNow;

            var msg = new Message()
            {
                Ip = ip.ToString(),
                DateTime = time,
                Text = value
            };
            
            _repository.Write(msg);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}