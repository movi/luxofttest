using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Client.Core.Interfaces;
using Moq;
using Xunit;

namespace Client.Core.UnitTests
{
    public class UnitTest1
    {
        [Fact]
        public void StartSendingOnStart()
        {
            var repoMock = new Mock<IMessagesRepository>();

            var unsent = new List<Message>()
            {
                new Message()
                {
                    ID = 1,
                    Text = "testOne"
                },
                new Message()
                {
                    ID = 2,
                    Text = "testTwo"
                }
            };

            repoMock.Setup(x => x.GetUnsentLines()).Returns(unsent);
            IMessagesRepository repository = repoMock.Object;


            var s = new Mock<ISender>();
            var sender = s.Object;

            var mp = new MessageProcessor(repository, sender);
            Task.Delay(5000);
            foreach (var msg in unsent)
            {
                //��� ����������� ����������, ���� �� �����, ���, ��������, ����� ���������
                s.Verify(m => m.Send(msg), Times.Once);
            }

            //TODO: ��� ��������� �����, ��� mp ����������� �� ������


            //var console = new ConsoleReader(writer, reader, s);
        }
    }
}
