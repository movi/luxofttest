﻿using System;
using System.Collections.Generic;
using System.Linq;
using Server.Core.Commands;
using Server.Core.Interfaces;

namespace Server.Core
{
    class Receiver
    {
        private IMessagesRepository _dbWriter;

        public Receiver(IMessagesRepository writer)
        {
            _dbWriter = writer ?? throw new ArgumentNullException(nameof(writer));
        }

        public void ParseLine(string line)
        {
            var commands = new List<ICommand>()
            {
                //сейчас два варианта - печать и сохранение строки в базу,
                // но т.к. уже два случая - это вполне может расшириться
                new PrintCommand(_dbWriter),
                new CommonCaseCommand(_dbWriter)
            };

            commands.ForEach(cmd => cmd.Parameter = (line));
            var command = commands.FirstOrDefault(cmd => cmd.CanBeExecuted);

            command?.Execute(line);
        }
    }

    interface ICommand
    {
        bool CanBeExecuted { get; }

        string Parameter {set; }

        void Execute(string arg);
    }
}
