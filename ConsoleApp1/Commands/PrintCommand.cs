﻿using System;
using Server.Core.Interfaces;

namespace Server.Core.Commands
{
    //тут, в принципе, больше ничего не делается, как формируется вывод
    //это уже визуальная часть
    class PrintCommand : ICommand
    {
        public PrintCommand(IMessagesRepository dbReader)
        {
            _dbReader = dbReader;
        }

        private string _arg;

        private IMessagesRepository _dbReader;

        public bool CanBeExecuted => String.Equals(_arg, "print", StringComparison.OrdinalIgnoreCase);

        public string Parameter { set => _arg = value; }

        public void Execute(string arg)
        {
          var lines =  _dbReader.ReadAllLines();

        }

    }
}
