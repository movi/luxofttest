﻿using System;
using Server.Core.Interfaces;

namespace Server.Core.Commands
{
    class CommonCaseCommand : ICommand
    {
        public CommonCaseCommand(IMessagesRepository writer)
        {
            _dbWriter = writer;
        }

        private string _arg;

        private IMessagesRepository _dbWriter;

        public bool CanBeExecuted {

            get
            {
                if (String.IsNullOrEmpty(_arg)) return false;
                return !String.Equals(_arg, "print", StringComparison.OrdinalIgnoreCase);
            }
        }

        public string Parameter { set => _arg = value; }

        public void Execute(string arg)
        {
            //писать в базу
            var msg = new Message(){ Text = arg};
            _dbWriter.Write(msg);
        }
    }
}
