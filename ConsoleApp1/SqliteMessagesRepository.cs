﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.Data.Sqlite;
using Server.Core.Interfaces;

namespace Server.Core
{
    public class SqliteMessagesRepository : IMessagesRepository
    {
        public IEnumerable<string> ReadAllLines()
        {
            var lines = new List<string>();

            using (var connection = new SqliteConnection("" +
                                                         new SqliteConnectionStringBuilder
                                                         {
                                                             DataSource = "messages.db"
                                                         }))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    //var insertCommand = connection.CreateCommand();
                    //insertCommand.Transaction = transaction;
                    //insertCommand.CommandText = "INSERT INTO message ( text ) VALUES ( $text )";
                    //insertCommand.Parameters.AddWithValue("$text", line.Text);
                    //insertCommand.ExecuteNonQuery();

                    var selectCommand = connection.CreateCommand();
                    selectCommand.Transaction = transaction;
                    selectCommand.CommandText = "SELECT text FROM messages";
                    using (var reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var message = reader.GetString(0);
                            lines.Add(message);
                        }
                    }

                    transaction.Commit();
                }

                return lines;
            }
        }

        public void Write(Message line)
        {
            using (var connection = new SqliteConnection("" +
                                                         new SqliteConnectionStringBuilder
                                                         {
                                                             DataSource = "messages.db"
                                                         }))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    var insertCommand = connection.CreateCommand();
                    insertCommand.Transaction = transaction;
                    insertCommand.CommandText = "INSERT INTO messages ( text ) VALUES ( $text )";
                    insertCommand.Parameters.AddWithValue("$text", $"[{line.Ip} {line.DateTime}]: {line.Text}"
                        );
                    insertCommand.ExecuteNonQuery();

                    transaction.Commit();
                }
            }
        }
    }
}