﻿using System.Collections.Generic;

namespace Server.Core.Interfaces
{
    public interface IMessagesRepository
    {
        IEnumerable<string> ReadAllLines();

        //запись строки в базу
        void Write(Message line);
    }
}