﻿using System;
using System.Linq;

namespace Server.Core
{
    class Program
    {
        static void Main(string[] args)
        {
            var r = new Random();
            Console.WriteLine("Hello World!");

            var writer = new SqliteMessagesRepository();
            var message = new Message()
            {
                Ip = "127.0.0.1",
                Text = "Kolerev"+r.Next(),
                DateTime = DateTime.UtcNow
            };
            writer.Write(message);

            var lines = new SqliteMessagesRepository().ReadAllLines().ToList();
            lines.ForEach(Console.WriteLine);

            Console.ReadKey();
        }
    }
}