﻿using System;

namespace Server.Core
{
    public class Message
    {
        public string Ip { get; set; }
        public DateTime DateTime { get; set; }

        public string Text { get; set; }
    }
}