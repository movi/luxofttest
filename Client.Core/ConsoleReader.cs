﻿using System;
using System.Collections.Generic;
using System.Text;
using Client.Core.Interfaces;

namespace Client.Core
{
    class ConsoleReader
    {
        public ConsoleReader(IMessagesRepository msgRepository, IMessageProcessor sender)
        {
            _msgRepository = msgRepository??throw new ArgumentNullException(nameof(msgRepository));

            _sender = sender ?? throw new ArgumentNullException(nameof(sender));

        }

        IMessagesRepository _msgRepository;
        IMessageProcessor _sender;

        public void Run()
        {
            string line = String.Empty;

            do
            {
                line = Console.ReadLine();

                if (!string.Equals(line, "print", StringComparison.OrdinalIgnoreCase))
                {
                    //Transaction
                    _msgRepository.Write(line);

                    int recordID = _msgRepository.LastRecordID;
                    var message = new Message()
                    {
                        ID = recordID,
                        Text = line
                    };
                    _sender.Add(message);
                }
                else
                {
                    var msgs = _sender.GetAllMessages();
                    Console.WriteLine("--------- Start printing all server messages -------------------");
                    foreach (var msg in msgs)
                    {

                        Console.WriteLine(msg);
                    }
                    Console.WriteLine("---------------- End of list  ---------------------------------");
                }
            }
            while (!String.Equals(line, @"/end", StringComparison.OrdinalIgnoreCase));
        }
    }
}
