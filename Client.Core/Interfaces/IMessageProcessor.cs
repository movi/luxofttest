﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Core.Interfaces
{
    interface IMessageProcessor
    {
        void Add(Message msg);
        IEnumerable<string> GetAllMessages();
    }
}
