﻿using System.Collections.Generic;

namespace Client.Core
{
    public interface ISender
    {
        void Send(Message msg);
        bool IsMessageSent(Message msg);

        IEnumerable<string> GetAllMessages();
    }
}