﻿using System.Collections.Generic;

namespace Client.Core.Interfaces
{
    // на старте приложения забрать
    // строки, которые еще не отправили
    //
    // зафиксировать, что отправка завершена успешно
    // и пометить запись соответствующим образом
    public interface IMessagesRepository
    {
        IEnumerable<string> ReadAllLines();
        List<Message> GetUnsentLines();
        
        //запись строки в базу
        void Write(string line);

        int LastRecordID { get; }

        /// <summary>
        /// Установить статус сообщения, что оно теперь отправлено
        /// </summary>
        /// <param name="msg"></param>
        void ChangeSentStatus(Message msg);
    }
}