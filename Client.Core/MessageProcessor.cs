﻿using Client.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace Client.Core
{
    public class MessageProcessor : IMessageProcessor
    {

        IMessagesRepository _msgRepository;
        ISender _sender;

        public MessageProcessor(IMessagesRepository reader, 
            ISender sender)
        {
            _msgRepository = reader;
            _sender = sender;

            _messages = new Queue<Message>();

            var lines = _msgRepository.GetUnsentLines();
            foreach (var l in lines)
            {
                Add(l);
            };

            Start();
        }

        private void Start()
        {
            Task.Run(ProcessQueue);
        }

        private async Task ProcessQueue()
        {
            while (true)
            {
                if (_messages.Count > 0)
                {
                    var msg = _messages.Dequeue();

                    _sender.Send(msg);
                    if (_sender.IsMessageSent(msg))
                    {
                        _msgRepository.ChangeSentStatus(msg);
                    }
                }
                await Task.Delay(500);
            }
        }

        public void Add(Message msg)
        {
            _messages.Enqueue(msg);
        }

        public IEnumerable<string> GetAllMessages()
        {
            return _sender.GetAllMessages();
        }

        private readonly Queue<Message> _messages;
    }
}
