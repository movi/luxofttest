﻿using System;
using Client.Core.Interfaces;

namespace Client.Core
{
    class Program
    {
        static void Main(string[] args)
        {
          
            var dbReader = new SqliteMessagesRepository();
            var sender = new ApiClient();
            var mp = new MessageProcessor(dbReader, sender);

            var consoleReader = new ConsoleReader(dbReader, mp);
            consoleReader.Run();
        }
    }
}