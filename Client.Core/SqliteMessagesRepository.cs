﻿using System.Collections.Generic;
using Microsoft.Data.Sqlite;

namespace Client.Core.Interfaces
{
    public class SqliteMessagesRepository : IMessagesRepository
    {
        public IEnumerable<string> ReadAllLines()
        {
            var lines = new List<string>();

            using (var connection = new SqliteConnection("" +
                                                         new SqliteConnectionStringBuilder
                                                         {
                                                             DataSource = "messages.db"
                                                         }))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    var selectCommand = connection.CreateCommand();
                    selectCommand.Transaction = transaction;
                    selectCommand.CommandText = "SELECT text FROM messages";
                    using (var reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var text = reader.GetString(0);
                            lines.Add(text);
                        }
                    }

                    transaction.Commit();
                }

                return lines;
            }
        }

        public List<Message> GetUnsentLines()
        {
            var lines = new List<Message>();

            using (var connection = new SqliteConnection("" +
                                                         new SqliteConnectionStringBuilder
                                                         {
                                                             DataSource = "messages.db"
                                                         }))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    var selectCommand = connection.CreateCommand();
                    selectCommand.Transaction = transaction;
                    selectCommand.CommandText = "SELECT id, message FROM messages where sent=0";
                    using (var reader = selectCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(0);
                            var text = reader.GetString(1);
                            var msg = new Message()
                            {
                                ID = id,
                                Text = text
                            };
                            lines.Add(msg);
                        }
                    }

                    transaction.Commit();
                }


            }
            return lines;
        }

        public void Write(string line)
        {
            using (var connection = new SqliteConnection("" +
                                                         new SqliteConnectionStringBuilder
                                                         {
                                                             DataSource = "messages.db"
                                                         }))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    var insertCommand = connection.CreateCommand();
                    insertCommand.Transaction = transaction;
                    insertCommand.CommandText = "INSERT INTO messages ( message ) VALUES ( $text )";
                    insertCommand.Parameters.AddWithValue("$text", line);
                    insertCommand.ExecuteNonQuery();

                    var selectCommand = connection.CreateCommand();
                    selectCommand.Transaction = transaction;
                    selectCommand.CommandText = "select last_insert_rowid()";

                    LastRecordID = (int)(long)selectCommand.ExecuteScalar();

                    transaction.Commit();
                }
            }
        }

        public int LastRecordID { get; private set; }

        public void ChangeSentStatus(Message msg)
        {
            using (var connection = new SqliteConnection("" +
                                                         new SqliteConnectionStringBuilder
                                                         {
                                                             DataSource = "messages.db"
                                                         }))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    var update = connection.CreateCommand();
                    update.Transaction = transaction;
                    update.CommandText = "Update messages set sent=1 where id=$id";
                    update.Parameters.AddWithValue("$id", msg.ID);
                    update.ExecuteNonQuery();

                    transaction.Commit();
                }
            }
        }
    }
}