﻿namespace Client.Core
{
    public class Message
    {
        public int ID { get; set; }

        public string Text { get; set; }
    }
}