﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Client.Core
{
    public class ApiClient : ISender
    {
        private string address = "http://localhost:5000";
        public void Send(Message msg)
        {
            var route = "/api/msg";
            //вообще очень хотелось Microsoft.AspNet.WebApi.Client, но у меня не получилось его
            //поставить на dotnetcore 1.0 /1.1, ждем версию 5.2.4
            //с fallbacks устанавливается, но не видно
            //возможно, работает с fallbacks на 2015 студии

            //We have determined that this issue is not a bug.
            //The latest released version of the Microsoft.AspNet.WebApi.Client package (5.2.3) 
            //does not directly support .NET Standard, so the imports/fallbacks must be specified. 
            //We are working on a new version (5.2.4) that will directly support .NET Standard.

            var httpClient = new HttpClient();
            var t = httpClient.PostAsync(
                address + route, //new StringContent(msg.Text, Encoding.UTF8, "application/json"));
                new JsonContent(msg.Text));

        }

        public IEnumerable<string> GetAllMessages()
        {
            var route = "/api/msg";
            var httpClient = new HttpClient();
            var t = httpClient.GetAsync(address + route);
            t.Wait();
           
            var response = t.Result;
            var jsonTask = response.Content.ReadAsStringAsync();
            jsonTask.Wait();
            var jsonString = jsonTask.Result;
            var result = JsonConvert.DeserializeObject<List<string>>(jsonString);
            return result;
        }

        public bool IsMessageSent(Message msg)
        {
            //TODO: это заглушка, будет время - опроси сервер на предмет
            //получено ли сообщение вообще

            //ответ OK для WebApi Client

            return true;
        }
    }

    public class JsonContent : StringContent
    {
        public JsonContent(object obj) :
            base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
        { }
    }
}